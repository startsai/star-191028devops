package com.demo.devops.leapyearcalculator1;

import junit.framework.TestCase;

public class FizzBuzzCalculatorTest extends TestCase {
	public void testReturn1_When_Given1() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(1);
		
		// Assert
		assertEquals("1", result);
	}
    
	public void testReturn2_When_Given2() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(2);
		
		// Assert
		assertEquals("2", result);
	}
    
	public void testReturnFizz_When_GivenMultpleOf3() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(3);
		
		// Assert
		assertEquals("Fizz", result);
	}

	public void testReturnBuzz_When_GivenMultpleOf5() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(5);
		
		// Assert
		assertEquals("Buzz", result);
	}
	
	public void testReturnFizzBuzz_When_GivenMultpleOf5And3() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(15);
		
		// Assert
		assertEquals("FizzBuzz", result);
	}

}
