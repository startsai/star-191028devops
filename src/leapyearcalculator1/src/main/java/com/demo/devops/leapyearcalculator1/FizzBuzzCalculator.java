package com.demo.devops.leapyearcalculator1;

public class FizzBuzzCalculator {
	public String getString(Integer value) {
		if (value % 3 == 0 && value % 5 ==0) {
			return "FizzBuzz";
		} else if (isMutipleOfThree(value)) {
			return "Fizz";
		} if (isMutipleOfFive(value)) {
			return "Buzz";
		} else {
		    return Integer.toString(value);
		}
	}
	
	private boolean isMutipleOfThree(Integer value) {
		return (value % 3 ==0);
	}

	private boolean isMutipleOfFive(Integer value) {
		return (value % 5 ==0);
	}
}
